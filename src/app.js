require('dotenv').config();

const express = require('express');
// Middleware for Loggers
const morgan = require('morgan');
// Middleware for secure headers
const helmet = require('helmet');
// Middleware - only be able to parse json and if the content-type header is of type json
const bodyParser = require('body-parser');

const middlewares = require('./middlewares');

const api = require('./api');
const db = require('./db');

const app = express();

app.use(helmet());
app.use(morgan('tiny'));
app.use(bodyParser.json());
app.use('/api', api);

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

module.exports = app;