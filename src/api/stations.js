const express = require('express');
const router = express.Router();

const Station = require('../models/station');

router.get('/', async (req, res, next) => {
  try {
    const stations = await Station.find();
    res.status(200).json(stations);
  } catch (error) {
    res.status(400);
    next(error);
  }
});

router.get('/:id', async (req, res, next) => {
  const { id } = req.params;

  try {
    const station = await Station.findById(id);
    res.status(200).json(station);
  } catch (error) {
    res.status(400);
    next(error);
  }
});

router.post('/', async (req, res, next) => {
  const station = new Station(req.body);
  try {
    const savedStation = await station.save();
    res.status(201).json(savedStation);
  } catch (error) {
    res.status(400);
    next(error);
  }
});

router.put('/:id', async (req, res, next) => {
  const { id } = req.params;
  try {
    const modifiedStation = await Station.findByIdAndUpdate(id, req.body);
    res.status(200).json(modifiedStation);
  } catch (error) {
    res.status(400);
    next(error);
  }
});

router.delete('/:id', async (req, res, next) => {
  const { id } = req.params;
  try {
    const deletedStation = await Station.findByIdAndDelete(id);
    res.status(200).json(deletedStation);
  } catch (error) {
    res.status(400);
    next(error);
  }
});

module.exports = router;