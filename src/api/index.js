const express = require('express');
const router = express.Router();

const stationsRouter = require('./stations');
const readingsRouter = require('./readings');

router.use('/stations', stationsRouter);
router.use('/readings', readingsRouter);

module.exports = router;