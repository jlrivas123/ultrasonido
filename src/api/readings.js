const express = require('express');
const router = express.Router();

const Reading = require('../models/reading');

router.get('/', async (req, res, next) => {
  try {
    const readings = await Reading.find().populate('station');
    res.status(200).json(readings);
  } catch (error) {
    res.status(400);
    next(error);
  }
});

router.get('/:id', async (req, res, next) => {
  const { id } = req.params;
  try {
    const reading = await Reading.findById(id).populate('station');
    res.status(200).json(reading);
  } catch (error) {
    res.status(400);
    next(error);
  }
});

router.post('/', async (req, res) => {
  const reading = new Reading(req.body);
  try {
    const savedReading = await reading.save();
    res.status(201).json(savedReading);
  } catch (error) {
    res.status(400);
    console.log(error);

    next(error);
  }
});

router.put('/:id', async (req, res, next) => {
  const { id } = req.params;
  try {
    const modifiedReading = await Reading.findByIdAndUpdate(id, req.body);
    res.status(200).json(modifiedReading);
  } catch (error) {
    res.status(400);
    next(error);
  }
});

router.delete('/:id', async (req, res, next) => {
  const { id } = req.params;
  try {
    const deletedReading = await Reading.findByIdAndDelete(id);
    res.status(200).json(deletedReading);
  } catch (error) {
    res.status(400);
    next(error);
  }
});

module.exports = router;