const mongoose = require('mongoose');

const stationsSchema = new mongoose.Schema({
  stationName: {
    type: String,
    required: true,
  },
  stationSeries: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: false,
  }
});

const Station = mongoose.model('Station', stationsSchema);

module.exports = Station;