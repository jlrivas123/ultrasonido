const mongoose = require('mongoose');

const readingsSchema = new mongoose.Schema({
  nodeSeries: {
    type: String,
    required: true,
  },
  nodeName: {
    type: String,
    required: true,
  },
  readingDay: {
    type: String,
    required: true,
  },
  readingHour: {
    type: String,
    required: true,
  },
  readingFullDate: { // readingDay + readingHour + .244Z => 2022-02-26T16:37:48.244Z
    type: Date,
    required: true,
  },
  distanceReading: {
    type: Number,
    required: true,
  },
  station: [{
    type: mongoose.Types.ObjectId,
    ref: 'Station',
    required: true,
  }],
  created_at    : { type: Date, required: true, default: Date.now }
});

const Reading = mongoose.model('Reading', readingsSchema);

module.exports = Reading;